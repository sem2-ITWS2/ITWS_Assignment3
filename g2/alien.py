import random


class alien():
    def __init__(self):
        self.y = random.randint(2, 3)
        self.x = 2 * random.randint(1, 8)

    def draw(self, stdscr):
        stdscr.addstr(self.y, self.x, "&")
        stdscr.refresh()

    def undraw(self, stdscr):
        stdscr.addstr(self.y, self.x, " ")
        stdscr.refresh()
