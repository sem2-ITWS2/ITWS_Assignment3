import curses
from curses import wrapper
import threading
import random
import sys
import os
from alien import *
from missile import *
from spaceship import *


flag = 1
score = -1


def game(stdscr):
    stdscr.clear()
    curses.curs_set(0)
    board(stdscr)
    increment(stdscr)
    create_alien(stdscr)
    ss = spaceship()
    ss.draw(stdscr)
    stdscr.refresh()
    while(True):
        c = stdscr.getch()
        if(c == ord('q')):
            curses.endwin()
            os._exit(0)
        ss.handler(stdscr, c)
        if(c == ord('i')):
            m1 = missile1()
            m1.start(stdscr, ss.x, ss.y)

        if(c == ord('s')):
            m2 = missile2()
            m2.start(stdscr, ss.x, ss.y)
        stdscr.refresh()

    curses.curs_set(1)


wrapper(game)
