import threading
import curses
from func import *


class missile():
    def __init__(self):
        self.x = 0
        self.y = 0

    def draw(self, stdscr, x, y, c):
        self.x = x
        self.y = y - 1
        stdscr.addstr(self.y, self.x, c)
        stdscr.refresh()


class missile1(missile):
    def update(self, stdscr):
        t = threading.Timer(1.0, self.update, [stdscr])
        t.start()

        if(stdscr.inch(self.y - 1, self.x) == ord('&')):
            t.cancel()
            increment(stdscr)
            stdscr.addstr(self.y - 1, self.x, " ")
            stdscr.refresh()

        if(self.y == 2):
            t.cancel()
            stdscr.addstr(self.y, self.x, " ")
            stdscr.refresh()

        else:
            self.y = self.y - 1
            stdscr.addstr(self.y + 1, self.x, " ")
            stdscr.addstr(self.y, self.x, "*")
            stdscr.refresh()

    def start(self, stdscr, x, y):
        self.draw(stdscr, x, y, "*")
        t = threading.Timer(1.0, self.update, [stdscr])
        t.start()


class missile2(missile):
    def update(self, stdscr):
        t = threading.Timer(1, self.update, [stdscr])
        t.start()

        if(stdscr.inch(self.y - 1, self.x) == ord('&')):
            increment(stdscr)
            stdscr.addstr(self.y - 1, self.x, "~")
            stdscr.addstr(self.y, self.x, " ")
            stdscr.refresh()
            t.cancel()
            return

        if(stdscr.inch(self.y - 2, self.x) == ord('&')):
            increment(stdscr)
            stdscr.addstr(self.y - 2, self.x, "~")
            stdscr.addstr(self.y, self.x, " ")
            stdscr.refresh()
            t.cancel()
            return

        if(self.y == 2 or self.y == 3):
            t.cancel()
            stdscr.addstr(self.y, self.x, " ")
            stdscr.refresh()

        else:
            self.y = self.y - 2
            stdscr.addstr(self.y + 2, self.x, " ")
            stdscr.addstr(self.y, self.x, "!")
            stdscr.refresh()

    def start(self, stdscr, x, y):
        self.draw(stdscr, x, y, "!")
        t = threading.Timer(1.0, self.update, [stdscr])
        t.start()
