import curses


class spaceship():
    def __init__(self):
        self.x = 2
        self.y = 9

    def draw(self, stdscr):
        stdscr.addstr(self.y, self.x, "^")
        stdscr.refresh()

    def handler(self, stdscr, c):
        if(c == ord('d') and self.x < 16):
            self.x += 2
            stdscr.addstr(self.y, self.x - 2, " ")
            self.draw(stdscr)
        elif(c == ord('a') and self.x > 2):
            self.x -= 2
            stdscr.addstr(self.y, self.x + 2, " ")
            self.draw(stdscr)
