import curses
import threading
from alien import *
score = -1


def board(stdscr):
    for i in range(2, 18, 2):
        stdscr.addstr(1, i, "-")
        stdscr.refresh()
    for i in range(2, 10, 1):
        stdscr.addstr(i, 1, "|")
        stdscr.refresh()
    for i in range(2, 18, 2):
        stdscr.addstr(10, i, "-")
        stdscr.refresh()
    for i in range(2, 10, 1):
        stdscr.addstr(i, 17, "|")
        stdscr.refresh()
    stdscr.addstr(4, 20, "A : Move spaceship left")
    stdscr.addstr(5, 20, "D : Move spaceship right")
    stdscr.addstr(6, 20, "I : Fire missile 1")
    stdscr.addstr(7, 20, "S : Fire missile 2")
    stdscr.addstr(8, 20, "Q : Quit")


def increment(stdscr):
    global score
    stdscr.addstr(2, 20, "SCORE : ")
    stdscr.addstr(2, 28, str(score + 1))
    score = score + 1
    stdscr.refresh()


def create_alien(stdscr):
    t = threading.Timer(10.0, create_alien, [stdscr])
    t.start()
    a = alien()
    a.draw(stdscr)
    t2 = threading.Timer(8.0, a.undraw, [stdscr])
    t2.start()
