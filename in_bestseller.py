import csv
import requests
from bs4 import BeautifulSoup
import string

outfile=open('output/in_com.csv',"w")
writer=csv.writer(outfile,delimiter=",")
writer.writerow(['Name','URL','Author','Price','Number of Ratings','Average Ratings'])
for i in range(1,6):
	
	url="https://www.amazon.in/gp/bestsellers/books/ref=zg_bs_pg_1?ie=UTF8&pg="+str(i)
#	print(url)
	page=requests.get(url)
	soup=BeautifulSoup(page.content,'html.parser')
	divs=soup.find_all('div', class_='zg_itemWrapper')
#	print(divs[0].prettify())
	x=0
	name=[]
	author=[]
	url=[]
	rating=[]
	number=[]
	price=[]
#writer.columns=['Name','URL','Author','Price','Number of Ratings','Average Ratings']
#writer.writerow()
#writer2=csv.DictWriter(writer,fieldnames=['Name','URL','Author','Price','Number of Ratings','Average Ratings'])

#writer2.writeheader()

	for i in divs:
		row=[]
#	print(i.find_all('div')[2].get_text())
		row.append(i.find_all('div')[2].get_text())	

#	print("https://www.amazon.in/",i.find_all('a')[0]['href'])
		s="https://www.amazon.in/"+i.find_all('a')[0]['href']
		row.append(s)
		if(i.find('a',class_='a-size-small a-link-child')!=None):
#		print(i.find('a',class_='a-size-small a-link-child').get_text())
			row.append(i.find('a',class_='a-size-small a-link-child').get_text())
		elif(i.find('span',class_='a-size-small a-color-base')!=None):
			row.append(i.find('span',class_='a-size-small a-color-base').get_text())
		else:
#		print("Not Available")
			row.append("Not Available")
		if(i.find('span',class_='currencyINR')!=None):
#		print(i.find('span',class_='a-size-base a-color-price').get_text())
			row.append(i.find('span',class_='a-size-base a-color-price').get_text())
		else:
#		print("Not Available")
			row.append("Not Available")
		if(i.find('a',class_='a-size-small a-link-normal')!=None):
#		print(i.find('a',class_='a-size-small a-link-normal').get_text())
			row.append(i.find('a',class_='a-size-small a-link-normal').get_text())
		else:
#		print("Not Available")
			row.append("Not Available")
		if(len(i.find_all('a',class_='a-link-normal'))>1 and i.find_all('a',class_='a-link-normal')[1].find_all('i')!=None and i.find_all('a',class_='a-link-normal')[1].has_attr('title')):
#		print(i.find_all('a',class_='a-link-normal')[1]['title'])
			row.append(i.find_all('a',class_='a-link-normal')[1]['title'])
		else:
			row.append("Not Available")
#		print("Not Available")
#	print(row)

		writer.writerow(row)

outfile.close()
