import csv
import requests
from bs4 import BeautifulSoup
import string

outfile = open('output/com_book.csv', "w")
writer = csv.writer(outfile, delimiter=",")
writer.writerow(['Name', 'URL', 'Author', 'Price',
                 'Number of Ratings', 'Average Ratings'])
for i in range(1, 6):

    url = "https://www.amazon.com/best-sellers-books-Amazon/zgbs/books/ref=zg_bs_pg_1?_encoding=UTF8&pg=" + \
        str(i)
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    divs = soup.find_all('div', class_='zg_itemWrapper')
    x = 0
    name = []
    author = []
    url = []
    rating = []
    number = []
    price = []
    for i in divs:
        row = []
        row.append(i.find_all('div')[2].get_text())

        s = "https://www.amazon.in/" + i.find_all('a')[0]['href']
        row.append(s)
        if(i.find('a', class_='a-size-small a-link-child') != None):
            row.append(
                i.find('a', class_='a-size-small a-link-child').get_text())
        elif(i.find('span', class_='a-size-small a-color-base') != None):
            row.append(
                i.find('span', class_='a-size-small a-color-base').get_text())
        else:
            row.append("Not Available")
        if(i.find('span', class_='p13n-sc-price') != None):
            row.append(
                i.find('span', class_='a-size-base a-color-price').get_text())
        else:
            row.append("Not Available")
        if(i.find('a', class_='a-size-small a-link-normal') != None):
            row.append(
                i.find('a', class_='a-size-small a-link-normal').get_text())
        else:
            row.append("Not Available")
        if(len(i.find_all('a', class_='a-link-normal')) > 1 and i.find_all('a', class_='a-link-normal')[1].find_all('i') != None and i.find_all('a', class_='a-link-normal')[1].has_attr('title')):
            row.append(i.find_all('a', class_='a-link-normal')[1]['title'])
        else:
            row.append("Not Available")

        writer.writerow(row)

outfile.close()
